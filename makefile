all: cruel_file

cruel_file: cruel_file.o

	ld -m elf_x86_64 -s -o cruel_file cruel_file.o

cruel_file.o: cruel_file.asm

	nasm -f elf64 cruel_file.asm
